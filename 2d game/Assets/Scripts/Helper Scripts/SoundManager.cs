﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    [SerializeField]
    public AudioClip landClip, deathClip, iceBreakClip, gameOverClip;
    [SerializeField]
    public AudioSource soundFX;
    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void LandSound()
    {
        soundFX.clip = landClip;
        soundFX.Play();
    }
    public void DeathSound()
    {
        soundFX.clip = deathClip;
        soundFX.Play();
    }
    public void iceBreakSound()
    {
        soundFX.clip = iceBreakClip;
        soundFX.Play();
    }
    public void gameOverSound()
    {
        soundFX.clip = gameOverClip;
        soundFX.Play();
    }
}
