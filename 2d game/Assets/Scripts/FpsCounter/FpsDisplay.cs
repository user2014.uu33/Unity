﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(FpsCounter))]

public class FpsDisplay : MonoBehaviour
{
    [SerializeField]
    private Text _label;

    private FpsCounter _fpsCounter;

    private void Awake()
    {
        _fpsCounter = GetComponent<FpsCounter>();
    }

    private void Update()
    {
        _label.text = Mathf.Clamp(_fpsCounter.FPS, 0, 60).ToString();
    }
}
