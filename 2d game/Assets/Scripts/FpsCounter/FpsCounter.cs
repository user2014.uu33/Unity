﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FpsCounter : MonoBehaviour
{
    [SerializeField]
    private int _farmeRange = 60;

    //average value
    private int[] _fpsBuffer;
    private int _fpsBufferIndex;

    public int FPS { get; private set; }

    private void Update()
    {
        //FPS = (int)(1f / Time.unscaledDeltaTime);
        if (_fpsBuffer == null || _farmeRange != _fpsBuffer.Length)
            InitializeBuffer();
        UpdateBuffer();
        CalculateFps();
    }

    private void InitializeBuffer()
    {
        if (_farmeRange <= 0)
            _farmeRange = 1;
        _fpsBuffer = new int[_farmeRange];
        _fpsBufferIndex = 0;
    }

    private void UpdateBuffer()
    {
        _fpsBuffer[_fpsBufferIndex++] = (int)(1f / Time.unscaledDeltaTime);
        if (_fpsBufferIndex >= _farmeRange)
            _fpsBufferIndex = 0;
    }

    private void CalculateFps()
    {
        int sum = 0;
        for (int i = 0; i < _farmeRange; ++i)
            sum += _fpsBuffer[i];
        FPS = sum / _farmeRange;
    }
}
